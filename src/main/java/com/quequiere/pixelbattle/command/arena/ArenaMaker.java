package com.quequiere.pixelbattle.command.arena;

import com.quequiere.pixelbattle.command.ConfigurableObject;

public class ArenaMaker extends ConfigurableObject{
	
	public String name;
	public int x1,y1,z1,x2,y2,z2=0;
	public int sx1,sy1,sz1,sx2,sy2,sz2=0;
	public boolean firstBlockDefine,secondBlockDefine,firstSpawnDefine,secondSpawnDefine = false;
	public String worldname;
	
	public ArenaMaker(String name)
	{
		this.name=name;
	}

	@Override
	public boolean isComplete()
	{
		if(this.name!=null && firstBlockDefine&&secondBlockDefine&&worldname!=null&&firstSpawnDefine&&secondSpawnDefine)
		{
			return true;
		}
		return false;
	}
	


}
