package com.quequiere.pixelbattle.command.arena;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.quequiere.pixelbattle.zone.Location;
import com.quequiere.pixelbattle.zone.physical.Arena;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.WorldServer;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;

public class ArenaCommand implements ICommand
{

	public HashMap<String, ArenaMaker> makers = new HashMap<String, ArenaMaker>();

	@Override
	public void processCommand(ICommandSender sender, String[] args)
	{
		if (!(sender instanceof EntityPlayer))
		{
			sender.addChatMessage(new ChatComponentText("Impossible depuis la console !"));
			return;
		}

		EntityPlayer p = (EntityPlayer) sender;

		if (args.length == 0)
		{
			p.addChatMessage(new ChatComponentText("/arena create [name]"));
			p.addChatMessage(new ChatComponentText("/arena cancel"));
			p.addChatMessage(new ChatComponentText("/arena print"));
			p.addChatMessage(new ChatComponentText("/arena save"));
			p.addChatMessage(new ChatComponentText("/arena resetpos"));
		}
		else if (args.length > 0)
		{
			String main = args[0];
			String pname = p.getCommandSenderName();
			ArenaMaker a = makers.get(pname);

			if (main.equals("create"))
			{
				if (a != null)
				{
					p.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Une arene est deja en cours d edition, pour l'annuler utiliser /arena cancel"));
				}
				else if (args.length == 2)
				{
					ArenaMaker am = new ArenaMaker(args[1]);
					makers.put(pname, am);
					p.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "------Arena maker info------"));
					for (String s : am.printFields())
					{
						p.addChatMessage(new ChatComponentText(EnumChatFormatting.BLUE + s));
					}
					p.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "--------------------"));
					p.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Veuillez maintenant cliquer sur les blocks definissant l'arene"));

				}
				else
				{
					p.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Specifier un nom svp."));
				}
			}
			else if (a != null)
			{
				if (main.equals("cancel"))
				{
					makers.remove(pname);
					p.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Arene annulee !"));
				}
				else if (main.equals("print"))
				{
					p.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "------Arena maker info------"));
					for (String s : a.printFields())
					{
						p.addChatMessage(new ChatComponentText(EnumChatFormatting.BLUE + s));
					}
					p.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "--------------------"));
				}
				else if (main.equals("save"))
				{
					if (a.isComplete())
					{
						WorldServer w = null;
						for (WorldServer wos : MinecraftServer.getServer().worldServers)
						{
							System.out.println(a.worldname + " VS " + wos.getWorldInfo().getWorldName());
							if (a.worldname.equals(wos.getWorldInfo().getWorldName()))
							{
								w = wos;
							}
						}

						Location l1 = new Location(a.x1, a.y1, a.z1, w);
						Location l2 = new Location(a.x2, a.y2, a.z2, w);
						Location sl1 = new Location(a.sx1, a.sy1, a.sz1, w);
						Location sl2 = new Location(a.sx2, a.sy2, a.sz2, w);
						Arena arena = new Arena(l1, l2, a.name, sl1, sl2);

						makers.remove(pname);

						if (Arena.addArena(arena))
						{
							arena.save();
							p.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Arene sauvegardee !"));
						}
						else
						{
							p.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Ce nom d'arene est deja pris"));
						}
					}
					else
					{
						p.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Cette arene n'est pas completement paramaitree"));
					}

				}
				else if (main.equals("resetpos"))
				{
					a.firstBlockDefine = false;
					a.secondBlockDefine = false;
					p.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Fait! Merci de redefinir les positions des mainetnant."));
				}
				else
				{
					p.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Mauvais argument"));
				}
			}
			else
			{
				p.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "utiliser /arena create [name] en premier !"));
			}
		}

	}

	@SubscribeEvent
	public void playerInteractEvent(PlayerInteractEvent e)
	{
		if (e.action.equals(PlayerInteractEvent.Action.RIGHT_CLICK_BLOCK) && FMLCommonHandler.instance().getEffectiveSide().equals(Side.SERVER))
		{

			EntityPlayer p = e.entityPlayer;
			ArenaMaker am = makers.get(p.getDisplayName());
			if (am != null)
			{
				if (!am.firstBlockDefine)
				{
					p.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Maintenant choississez le second block"));
					am.firstBlockDefine = true;
					am.x1 = e.x;
					am.y1 = e.y;
					am.z1 = e.z;
					am.worldname = p.worldObj.getWorldInfo().getWorldName();
				}
				else if (!am.secondBlockDefine)
				{
					if (p.getEntityWorld().getWorldInfo().getWorldName().equals(am.worldname))
					{
						p.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Merci de definir le spawn 1 de l'arene"));
						am.secondBlockDefine = true;
						am.x2 = e.x;
						am.y2 = e.y;
						am.z2 = e.z;
					}
					else
					{
						p.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Don't think about that ! It's bad idea. Arena can't be on 2 worlds"));
					}

				}
				else if (!am.firstSpawnDefine)
				{
					if (p.getEntityWorld().getWorldInfo().getWorldName().equals(am.worldname))
					{
						p.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Merci de definir de spawn 2 de l'arene"));
						am.firstSpawnDefine = true;
						am.sx1 = e.x;
						am.sy1 = e.y;
						am.sz1 = e.z;
					}
					else
					{
						p.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Don't think about that ! It's bad idea. Arena can't be on 2 worlds"));
					}

				}
				else if (!am.secondSpawnDefine)
				{
					if (p.getEntityWorld().getWorldInfo().getWorldName().equals(am.worldname))
					{
						p.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Creation terminee, vous pouvez sauvegarder avec /arena save"));
						am.secondSpawnDefine = true;
						am.sx2 = e.x;
						am.sy2 = e.y;
						am.sz2 = e.z;
					}
					else
					{
						p.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "Don't think about that ! It's bad idea. Arena can't be on 2 worlds"));
					}

				}
				else
				{
					p.addChatMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Creation deja terminee, vous pouvez sauvegarder avec /arena save !"));
				}
			}
		}

	}

	@Override
	public int compareTo(Object paramT)
	{
		return 0;
	}

	@Override
	public String getCommandName()
	{
		return "arena";
	}

	@Override
	public String getCommandUsage(ICommandSender p_71518_1_)
	{
		return "arena [subcommand]";
	}

	@Override
	public List getCommandAliases()
	{
		return null;
	}

	@Override
	public boolean canCommandSenderUseCommand(ICommandSender p_71519_1_)
	{
		return true;
	}

	@Override
	public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_)
	{
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] p_82358_1_, int p_82358_2_)
	{
		return false;
	}

	public static boolean doesStringStartWith(String p_71523_0_, String p_71523_1_)
	{
		return p_71523_1_.regionMatches(true, 0, p_71523_0_, 0, p_71523_0_.length());
	}

}
