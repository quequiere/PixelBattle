package com.quequiere.pixelbattle.command;

import java.lang.reflect.Field;
import java.util.ArrayList;


public abstract class ConfigurableObject {
	
	public ArrayList<String> printFields()
	{
		ArrayList<String> list = new ArrayList<String>();
		
		for(Field f:this.getClass().getDeclaredFields())
		{
			try {
				list.add(f.getName()+" : "+f.get(this));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		
		return list;
		
	}
	
	public abstract boolean isComplete();

}
