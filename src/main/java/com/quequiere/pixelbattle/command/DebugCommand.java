package com.quequiere.pixelbattle.command;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.quequiere.pixelbattle.PixelBattle;
import com.quequiere.pixelbattle.organisation.AffrontementType;
import com.quequiere.pixelbattle.organisation.Duel;
import com.quequiere.pixelbattle.organisation.livraison.RecompenseExp;
import com.quequiere.pixelbattle.organisation.multi.Tournoi;
import com.quequiere.pixelbattle.zone.Location;
import com.quequiere.pixelbattle.zone.physical.Arena;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.relauncher.Side;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.WorldServer;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;

public class DebugCommand implements ICommand
{


	@Override
	public void processCommand(ICommandSender sender, String[] args)
	{
		EntityPlayer p1 = PixelBattle.getPlayer("quequiere");
		EntityPlayer p2 = PixelBattle.getPlayer("chamalier");
		EntityPlayer p3 = PixelBattle.getPlayer("truc");
		
		//Duel duel = Duel.initializeDuel(p1, p2, AffrontementType.pvp);
		//duel.addRecompense(new RecompenseExp(50));
		//duel.start(Arena.getFreeArena());
		
		Tournoi t = new Tournoi(5);
		t.subscribe(p1);
		t.subscribe(p2);
		t.subscribe(p3);
	}



	@Override
	public int compareTo(Object paramT)
	{
		return 0;
	}

	@Override
	public String getCommandName()
	{
		return "debug";
	}

	@Override
	public String getCommandUsage(ICommandSender p_71518_1_)
	{
		return "debug [subcommand]";
	}

	@Override
	public List getCommandAliases()
	{
		return null;
	}

	@Override
	public boolean canCommandSenderUseCommand(ICommandSender p_71519_1_)
	{
		return true;
	}

	@Override
	public List addTabCompletionOptions(ICommandSender p_71516_1_, String[] p_71516_2_)
	{
		return null;
	}

	@Override
	public boolean isUsernameIndex(String[] p_82358_1_, int p_82358_2_)
	{
		return false;
	}

	public static boolean doesStringStartWith(String p_71523_0_, String p_71523_1_)
	{
		return p_71523_1_.regionMatches(true, 0, p_71523_0_, 0, p_71523_0_.length());
	}

}
