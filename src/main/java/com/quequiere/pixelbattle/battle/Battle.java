package com.quequiere.pixelbattle.battle;

import com.quequiere.pixelbattle.organisation.Affrontement;

import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraftforge.common.MinecraftForge;

public abstract class Battle {
	
	private Affrontement affrontement;

	public Battle(Affrontement affrontement)
	{
		this.affrontement = affrontement;
		this.registerEvent();
	}
	
	public void registerEvent()
	{
		 FMLCommonHandler.instance().bus().register(this);
		 MinecraftForge.EVENT_BUS.register(this);
	}

	public Affrontement getAffrontement()
	{
		return affrontement;
	}
	
	public void giveWinerAndClose(boolean firstTeamWin)
	{
		FMLCommonHandler.instance().bus().unregister(this);
		MinecraftForge.EVENT_BUS.unregister(this);
		this.getAffrontement().win(firstTeamWin);
	}

}
