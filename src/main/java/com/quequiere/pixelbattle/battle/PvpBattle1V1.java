package com.quequiere.pixelbattle.battle;

import com.quequiere.pixelbattle.organisation.Affrontement;
import com.quequiere.pixelbattle.organisation.Duel;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;

public class PvpBattle1V1 extends Battle 
{

	public PvpBattle1V1(Affrontement affrontement)
	{
		super(affrontement);	
	}

	
	@SubscribeEvent
	public void livingDeathEvent(LivingDeathEvent e)
	{
		if(e.entityLiving instanceof EntityPlayer)
		{
			if(this.getAffrontement() instanceof Duel)
			{
				Duel d = (Duel) this.getAffrontement();
				if(e.entityLiving.equals(d.getP1()))
				{
					this.giveWinerAndClose(false);
				}
				else if(e.entityLiving.equals(d.getP2()))
				{
					this.giveWinerAndClose(true);
				}
			}
		}
	}

	
	//renvoi le gagnant ou perdant
}
