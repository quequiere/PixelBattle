package com.quequiere.pixelbattle;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.common.MinecraftForge;

import java.io.File;

import com.quequiere.pixelbattle.command.DebugCommand;
import com.quequiere.pixelbattle.command.arena.ArenaCommand;
import com.quequiere.pixelbattle.organisation.multi.Tournoi;
import com.quequiere.pixelbattle.thread.ThreadWatcher;
import com.quequiere.pixelbattle.zone.physical.Arena;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;

@Mod(modid = PixelBattle.MODID, version = PixelBattle.VERSION)
public class PixelBattle
{
	public static final String MODID = "PixelBattle";
	public static final String VERSION = "1.0";

	protected static PixelBattle pixelBattle;

	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		pixelBattle = this;
	}

	@EventHandler
	public void serverLoad(FMLServerStartingEvent event)
	{

		FMLCommonHandler.instance().bus().register(new ThreadWatcher());

		ArenaCommand arenacommand = new ArenaCommand();
		event.registerServerCommand(arenacommand);
		MinecraftForge.EVENT_BUS.register(arenacommand);

		DebugCommand debugCommand = new DebugCommand();
		event.registerServerCommand(debugCommand);

		Arena.loadArenaFromFile();

		// FMLCommonHandler.instance().bus().register(new SkcEvent());
		// MinecraftForge.EVENT_BUS.register(new SkcEvent());
	}

	public static EntityPlayerMP getPlayer(String name)
	{
		for (Object o : MinecraftServer.getServer().getConfigurationManager().playerEntityList)
		{
			EntityPlayerMP p = (EntityPlayerMP) o;
			if (p.getDisplayName().equals(name))
			{
				return p;
			}
		}
		return null;
	}
}
