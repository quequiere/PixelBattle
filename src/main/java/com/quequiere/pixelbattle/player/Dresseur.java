package com.quequiere.pixelbattle.player;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;

public class Dresseur
{

	private static ArrayList<Dresseur> dresseurLoaded = new ArrayList<Dresseur>();

	private int experience;
	private String name;

	private Dresseur(String name)
	{
		this.name = name;
	}

	public static Dresseur getDresseur(String name)
	{
		for (Dresseur d : dresseurLoaded)
		{
			if (d.getName().equals(name))
			{
				return d;
			}
		}
		
		return loadDresseurFromFile(name);
	}

	public int getExperience()
	{
		return experience;
	}

	public void setExperience(int experience)
	{
		this.experience = experience;
		this.save();
	}

	public String getName()
	{
		return name;
	}

	private String toJson()
	{
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	private static Dresseur fromJson(String s)
	{
		Gson gson = new Gson();
		return gson.fromJson(s, Dresseur.class);
	}

	public void save()
	{

		File f = new File("./config/PixelBattle/" + Dresseur.class.getSimpleName() + "/" + this.getName() + ".json");

		try
		{
			if (!f.getParentFile().exists())
			{
				f.getParentFile().mkdirs();
			}

			if (!f.exists())
			{
				f.createNewFile();
			}

			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(this.toJson());
			bw.close();
			fw.close();

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	
	}

	private static Dresseur loadDresseurFromFile(String name)
	{
		Dresseur d=null;
		File dir = new File("./config/PixelBattle/" + Dresseur.class.getSimpleName());

		if (!dir.exists())
		{
			dir.mkdirs();
		}

		File dresseurFile = new File(dir.getPath() + "/" + name + ".json");

		if (!dresseurFile.exists())
		{
			d = new Dresseur(name);
			d.save();
			dresseurLoaded.add(d);
		}
		else
		{

			BufferedReader br = null;

			try
			{

				String sCurrentLine;
				String total = "";

				br = new BufferedReader(new FileReader(dresseurFile));

				while ((sCurrentLine = br.readLine()) != null)
				{
					total += sCurrentLine;
				}

				d = Dresseur.fromJson(total);
				dresseurLoaded.add(d);

			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					if (br != null)
						br.close();
				}
				catch (IOException ex)
				{
					ex.printStackTrace();
				}
			}

		}
		
		return d;
	}

}
