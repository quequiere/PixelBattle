package com.quequiere.pixelbattle.player;

public enum Classement {

	Bronze,
	Argent,
	Or,
	Platine,
	Diamant;
	
}
