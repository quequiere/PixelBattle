package com.quequiere.pixelbattle.organisation;

import java.util.ArrayList;

import net.minecraft.entity.player.EntityPlayer;

public abstract class AffrontementWinEvent 
{
	public abstract void onAffrontementWinEvent(Affrontement a,ArrayList<EntityPlayer> gagnants,ArrayList<EntityPlayer> perdants);
}
