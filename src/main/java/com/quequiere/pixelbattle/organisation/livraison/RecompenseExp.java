package com.quequiere.pixelbattle.organisation.livraison;

import com.quequiere.pixelbattle.player.Dresseur;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class RecompenseExp extends Recompense {

	int expToAdd = 0;
	
	public RecompenseExp(int expToAdd)
	{
		this.expToAdd=expToAdd;
	}
	
	@Override
	public void livrer(EntityPlayer p)
	{
		Dresseur d = Dresseur.getDresseur(p.getDisplayName());
		d.setExperience(d.getExperience()+this.expToAdd);
		p.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GREEN+"Vous avez obtenu "+this.expToAdd+" exp ! "));
		p.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GREEN+"Vous avez maintenant "+d.getExperience()+" exp."));
	}

}
