package com.quequiere.pixelbattle.organisation.multi;

import java.util.ArrayList;

import com.quequiere.pixelbattle.organisation.Affrontement;
import com.quequiere.pixelbattle.organisation.AffrontementType;
import com.quequiere.pixelbattle.organisation.AffrontementWinEvent;
import com.quequiere.pixelbattle.organisation.Duel;
import com.quequiere.pixelbattle.thread.ThreadWatcher;
import com.quequiere.pixelbattle.zone.physical.Arena;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import scala.Array;

public class Tournoi
{

	public static ArrayList<Tournoi> tournois = new ArrayList<Tournoi>();

	private boolean isReady = false;
	private int preparationTimeInSecond;
	private ArrayList<Participant> participants = new ArrayList<Participant>();
	private ArrayList<Duel> duels = new ArrayList<Duel>();

	public Tournoi(int prepaTimeSec)
	{
		this.preparationTimeInSecond = prepaTimeSec;
		tournois.add(this);
		startGestionOuverture(this.preparationTimeInSecond);

	}

	public boolean subscribe(EntityPlayer p)
	{
		if (isReady)
		{
			return false;
		}

		for (Tournoi t : tournois)
		{
			if(t.getParticipantByName(p.getDisplayName())!=null)
			{
				p.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.RED+"Vous n'avez pas ete ajoute au tournoi !"));
				return false;
			}

		}
		this.participants.add(new Participant(p.getDisplayName(),this));
		p.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GREEN+"Vous avez bien ete ajoute au tournoi !"));
		return true;
	}

	public void searchForOpponent()
	{
		class T extends Thread
		{
			public Tournoi tournoi;

			public T(Tournoi t)
			{
				this.tournoi = t;

			}

			public void run()
			{
				//Ne lance qu'un duel toutes les secondes !
				Arena a = Arena.getFreeArena();
				if(a==null)
				{
					return;
				}
		
				ArrayList<Participant> freepar = this.tournoi.getFreeParticipant();
				if(freepar.size()<2)
				{
					return;
				}
				
				Participant p1=null;
				Participant p2=null;
				int scan = 0;
				
				for(Participant oripar:freepar)
				{
					p1=oripar;
					p2=null;
					
					for(int x=scan;x<freepar.size();x++)
					{
						Participant secpar = freepar.get(x);
						if(secpar!=oripar && oripar.canFightWith(secpar.getName()))
						{
							p2=secpar;
							break;
						}
					}
					
					if(p2!=null)
					{
						break;
					}
					scan++;
				}
				
				if(p1==null || p2==null)
				{
					return;
				}
				
				System.out.println(p1.getName()+" VS "+p2.getName());
				
				
				Duel d = Duel.initializeDuel(p1.getPlayer(), p2.getPlayer(), AffrontementType.pvp);
				duels.add(d);
				d.registerListener(new AffrontementWinEvent()
				{
					@Override
					public void onAffrontementWinEvent(Affrontement a, ArrayList<EntityPlayer> gagnants, ArrayList<EntityPlayer> perdants)
					{
						for(EntityPlayer p:gagnants)
						{
							Participant par = tournoi.getParticipantByName(p.getDisplayName());
							par.addVictory();
							for(EntityPlayer perd:perdants)
							{
								par.addAversaire(perd.getDisplayName());
							}
							p.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GREEN+"Une victoire vous a ete ajoute pour le tournoi !"));
							p.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GREEN+"Nous vous recherchons un nouvel adversaire !"));
						}
						
						for(EntityPlayer p:perdants)
						{
							Participant par = tournoi.getParticipantByName(p.getDisplayName());
							for(EntityPlayer gagn:gagnants)
							{
								par.addAversaire(gagn.getDisplayName());
							}
							p.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GREEN+"Nous vous recherchons un nouvel adversaire !"));
						}
						
						tournoi.duels.remove(a);
					}
				});
				d.start(a);
				System.out.println("Un combat tournoi a demare !");

			}
		}
		ThreadWatcher.register(new T(this));
	}
	
	public ArrayList<Participant> getFreeParticipant()
	{
		 ArrayList<Participant> list = new  ArrayList<Participant>();
		 for(Participant par:this.getParticipants())
		 {
			 if(!par.isInFight()&&par.isOnline()&&par.getPlayer().isEntityAlive())
			 {
				 list.add(par);
			 }
		 }
		 return list;
	}

	public void startGestionOuverture(int timeInSeconds)
	{

		class T extends Thread
		{
			public Tournoi tournoi;
			public double parcouru = 0d;
			public double lastPercent = 0d;

			public T(Tournoi t)
			{
				this.tournoi = t;
			}

			public void run()
			{
				parcouru++;
				double percent = parcouru / this.tournoi.preparationTimeInSecond;
				System.out.println(percent);

				if (percent >= 1)
				{
					this.tournoi.isReady = true;
					ThreadWatcher.unregister(this);
					System.out.println("Compte a rebour fini !");
					this.tournoi.searchForOpponent();
				}
				else
				{

					boolean display = false;

					if (percent >= 0.5d && lastPercent < 0.5d)
					{
						lastPercent = 0.5d;
						display = true;
					}
					else if (percent >= 0.7d && lastPercent < 0.7d)
					{
						lastPercent = 0.7d;
						display = true;
					}
					else if (percent >= 0.9d && lastPercent < 0.9d)
					{
						lastPercent = 0.9d;
						display = true;
					}
					else if (percent >= 0.95d && lastPercent < 0.95d)
					{
						lastPercent = 0.95d;
						display = true;
					}

					if (display)
					{
						double reste = this.tournoi.preparationTimeInSecond - parcouru;
						System.out.println("Il ne reste que " + reste + " secondes pour s'inscrire !");
					}
				}

			}
		}

		ThreadWatcher.register(new T(this));
	}

	public ArrayList<Participant> getParticipants()
	{
		return participants;
	}

	public ArrayList<Duel> getDuels()
	{
		return duels;
	}
	
	public Participant getParticipantByName(String name)
	{
		for(Participant par:this.getParticipants())
		{
			if(par.getName().equals(name))
			{
				return par;
			}
		}
		return null;
	}

	
}
