package com.quequiere.pixelbattle.organisation.multi;

import java.util.ArrayList;

import com.quequiere.pixelbattle.PixelBattle;
import com.quequiere.pixelbattle.organisation.Duel;

import net.minecraft.entity.player.EntityPlayer;

public class Participant
{
	private String name;
	private int victoryNb;
	private ArrayList<String> adversaire = new ArrayList<String>();
	private Tournoi tournoi;

	public Participant(String name, Tournoi tournoi)
	{
		this.name = name;
		this.tournoi = tournoi;
	}

	public int getVictoryNb()
	{
		return victoryNb;
	}

	public void addVictory()
	{
		this.victoryNb++;
	}

	public String getName()
	{
		return name;
	}

	public boolean isInFight()
	{
		for (Duel d : this.tournoi.getDuels())
		{
			if (d.containPlayer(this.name))
			{
				return true;
			}
		}

		return false;
	}

	public boolean isOnline()
	{
		if (PixelBattle.getPlayer(this.name) == null)
		{
			return false;
		}
		return true;
	}

	public EntityPlayer getPlayer()
	{
		return PixelBattle.getPlayer(this.name);
	}

	public void addAversaire(String name)
	{
		this.adversaire.add(name);
	}

	public boolean canFightWith(ArrayList<String> names)
	{
		for (String n : names)
		{
			if (this.adversaire.contains(n))
			{
				return false;
			}
		}
		return true;
	}

	public boolean canFightWith(String na)
	{
		if (this.adversaire.contains(na))
		{
			return false;
		}

		return true;
	}

	public ArrayList<String> getAdversaire()
	{
		return adversaire;
	}
	
	

}
