package com.quequiere.pixelbattle.organisation;

import java.util.ArrayList;

import com.quequiere.pixelbattle.battle.PvpBattle1V1;
import com.quequiere.pixelbattle.zone.physical.Arena;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;

public class Duel extends Affrontement
{

	
	private Duel(ArrayList<EntityPlayer> t1,ArrayList<EntityPlayer> t2,AffrontementType type)
	{
		super(t1, t2, type);
	}
	
	public static Duel initializeDuel(EntityPlayer p1, EntityPlayer p2, AffrontementType type)
	{
		ArrayList<EntityPlayer> t1 = new ArrayList<EntityPlayer>();
		ArrayList<EntityPlayer> t2 = new ArrayList<EntityPlayer>();
		t1.add(p1);
		t2.add(p2);
		return new Duel(t1, t2, type);
	}
	
	public EntityPlayer getP1()
	{
		return this.getTeam1().get(0);
	}
	
	public EntityPlayer getP2()
	{
		return this.getTeam2().get(0);
	}
	


	@Override
	public void start(Arena a)
	{
		super.start(a);
		
		boolean success = false;
		
		if(this.getType().equals(AffrontementType.pvp))
		{
			PvpBattle1V1 pvp = new PvpBattle1V1(this);
			success=true;
		}
		else
		{
			
		}
		
		if(success)
		{
			a.getSpawn1().teleport(this.getP1(), true);
			a.getSpawn2().teleport(this.getP2(), true);
			this.getP1().addChatMessage(new ChatComponentText("Nous vous teleportons a l'arene !"));
			this.getP2().addChatMessage(new ChatComponentText("Nous vous teleportons a l'arene !"));
		}
	}
	
}
