package com.quequiere.pixelbattle.organisation;

import java.util.ArrayList;

import com.quequiere.pixelbattle.organisation.livraison.Recompense;
import com.quequiere.pixelbattle.zone.physical.Arena;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public abstract class Affrontement
{

	private ArrayList<EntityPlayer> team1, team2;
	private AffrontementType type;
	private Arena arenaInUse;
	private ArrayList<Recompense> recompense = new ArrayList<Recompense>();
	private ArrayList<AffrontementWinEvent> listeners = new ArrayList<AffrontementWinEvent>();

	public Affrontement(ArrayList<EntityPlayer> t1, ArrayList<EntityPlayer> t2, AffrontementType type)
	{
		this.team1 = t1;
		this.team2 = t2;
		this.type = type;
	}
	

	// different en foction du type d'affrontement
	public void start(Arena a)
	{
		this.arenaInUse = a;
	}
	
	public boolean containPlayer(String name)
	{
		for(EntityPlayer p:team1)
		{
			if(p.getDisplayName().equals(name))
			{
				return true;
			}
		}
		
		for(EntityPlayer p:team2)
		{
			if(p.getDisplayName().equals(name))
			{
				return true;
			}
		}
		
		return false;
	}

	public ArrayList<Recompense> getRecompense()
	{
		return recompense;
	}
	
	public void addRecompense(Recompense r)
	{
		this.getRecompense().add(r);
	}

	public ArrayList<EntityPlayer> getTeam1()
	{
		return team1;
	}

	public ArrayList<EntityPlayer> getTeam2()
	{
		return team2;
	}

	public AffrontementType getType()
	{
		return type;
	}

	public Arena getArenaInUse()
	{
		return arenaInUse;
	}

	public void setArenaInUse(Arena arenaInUse)
	{
		this.arenaInUse = arenaInUse;
	}

	public void win(boolean firstTeamWin)
	{
	
		
		this.arenaInUse = null;

		ArrayList<EntityPlayer> gagnant = new ArrayList<EntityPlayer>();
		ArrayList<EntityPlayer> perdant = new ArrayList<EntityPlayer>();

		if (firstTeamWin)
		{
			gagnant = this.getTeam1();
			perdant = this.getTeam2();
		}
		else
		{
			gagnant = this.getTeam2();
			perdant = this.getTeam1();
		}
		
		for(AffrontementWinEvent e:this.listeners)
		{
			e.onAffrontementWinEvent(this, gagnant,perdant);
		}

		for (EntityPlayer p : gagnant)
		{
			p.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.GREEN + "Vous avez gagne ce duel !"));
			for(Recompense r:this.getRecompense())
			{
				r.livrer(p);
			}
		}

		for (EntityPlayer p : perdant)
		{
			p.addChatComponentMessage(new ChatComponentText(EnumChatFormatting.RED + "Vous avez perdu ce duel !"));
		}
	}
	
	public void registerListener(AffrontementWinEvent event)
	{
		this.listeners.add(event);
	}

}
