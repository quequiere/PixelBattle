package com.quequiere.pixelbattle.zone.physical;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.quequiere.pixelbattle.battle.Battle;
import com.quequiere.pixelbattle.zone.Location;
import com.quequiere.pixelbattle.zone.Zone;

public class Arena extends Zone
{

	private static ArrayList<Arena> arenaList = new ArrayList<Arena>();
	private Location spawn1, spawn2;

	public Arena(Location l1, Location l2, String name, Location s1, Location s2)
	{
		super(l1, l2, name);
		this.spawn1 = s1;
		this.spawn2 = s2;
	}

	private boolean isFree()
	{
		return true;
	}
	
	
	
	
	
	
	
	


	// static Function
	public static ArrayList<Arena> getAllArena()
	{
		return arenaList;
	}
	
	public static Arena getFreeArena()
	{
		for(Arena a:getAllArena())
		{
			if(a.isFree())
			{
				return a;
			}
		}
		return null;
	}

	public static boolean addArena(Arena a)
	{
		for (Arena aa : getAllArena())
		{
			if (aa.getName().equals(a.getName()))
			{
				return false;
			}
		}

		getAllArena().add(a);
		return true;
	}

	public static void removeArena(Arena a)
	{
		getAllArena().remove(a);
	}

	public static Arena fromJson(String s)
	{
		Gson gson = new Gson();
		return gson.fromJson(s, Arena.class);
	}

	public static void loadArenaFromFile()
	{
		File dir = new File("./config/PixelBattle/" + Arena.class.getSimpleName());
		
		if(!dir.exists())
		{
			dir.mkdirs();
		}
		
		for (File f : dir.listFiles())
		{
			if (!f.isDirectory())
			{
				BufferedReader br = null;

				try
				{

					String sCurrentLine;
					String total = "";

					br = new BufferedReader(new FileReader(f));

					while ((sCurrentLine = br.readLine()) != null)
					{
						total += sCurrentLine;
					}

					Arena a = Arena.fromJson(total);
					if (!Arena.addArena(a))
					{
						System.out.println("Error on " + a.getName() + " can't be added, already exist !");
					}

				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
				finally
				{
					try
					{
						if (br != null)
							br.close();
					}
					catch (IOException ex)
					{
						ex.printStackTrace();
					}
				}
			}
		}

		System.out.println(Arena.getAllArena().size() + " arene chargees !");

	}

	public Location getSpawn1()
	{
		return spawn1;
	}

	public Location getSpawn2()
	{
		return spawn2;
	}
	
	

}
