package com.quequiere.pixelbattle.zone;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;

import net.minecraft.entity.player.EntityPlayer;

public abstract class Zone
{

	protected Location l1, l2;
	protected String name;

	public Zone(Location l1, Location l2, String name)
	{
		this.l1 = l1;
		this.l2 = l2;
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public Location getL1()
	{
		return l1;
	}

	public Location getL2()
	{
		return l2;
	}

	public Location getMinLoc()
	{
		return new Location(Math.min(this.l1.getX(), this.l2.getX()), Math.min(this.l1.getY(), this.l2.getY()), Math.min(this.l1.getZ(), this.l2.getY()), this.l1.getWorld());
	}

	public Location getMaxLoc()
	{
		return new Location(Math.max(this.l1.getX(), this.l2.getX()), Math.max(this.l1.getY(), this.l2.getY()), Math.max(this.l1.getZ(), this.l2.getY()), this.l1.getWorld());
	}

	public boolean isInZone(EntityPlayer p)
	{
		Location l = new Location(p.posX, p.posY, p.posZ, p.worldObj);
		if (this.isInZone(l))
		{
			return true;
		}
		return false;
	}

	public boolean isInZone(Location l)
	{
		if (this.getL1().getWorld().equals(l.getWorld()))
		{
			return false;
		}

		Location max = this.getMaxLoc();
		Location min = this.getMinLoc();

		if (min.getX() <= l.getX() && l.getX() < max.getX())
		{
			if (min.getY() <= l.getY() && l.getY() < max.getY())
			{
				if (min.getZ() <= l.getZ() && l.getZ() < max.getZ())
				{
					return true;
				}
			}
		}

		return false;
	}

	public String toJson()
	{
		Gson gson = new Gson();
		return gson.toJson(this);
	}

	public void save()
	{
		File f = new File("./config/PixelBattle/" + this.getClass().getSimpleName() + "/" + this.getName() + ".json");

		try
		{
			if (!f.getParentFile().exists())
			{
				f.getParentFile().mkdirs();
			}

			if (!f.exists())
			{
				f.createNewFile();
			}

			FileWriter fw = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(this.toJson());
			bw.close();
			fw.close();

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}
}
