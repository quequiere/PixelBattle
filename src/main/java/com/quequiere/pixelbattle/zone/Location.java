package com.quequiere.pixelbattle.zone;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public class Location
{

	private double x, y, z;
	private String w;

	public Location(double x, double y, double z, World w)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.w = w.getWorldInfo().getWorldName();
	}

	public double getX()
	{
		return x;
	}

	public double getY()
	{
		return y;
	}

	public double getZ()
	{
		return z;
	}

	public World getWorld()
	{
		for (WorldServer wos : MinecraftServer.getServer().worldServers)
		{
			if (this.w.equals(wos.getWorldInfo().getWorldName()))
			{
				return wos;
			}
		}
		return null;
	}

	@Override
	public boolean equals(Object obj)
	{

		if (obj instanceof Location)
		{
			Location l = (Location) obj;
			if (l.getX() == this.getX() && l.getY() == this.getY() && l.getZ() == this.getZ() && l.getWorld().equals(this.getWorld()))
			{
				return true;
			}
		}

		return false;
	}
	
	public void teleport(EntityPlayer p,boolean moreY)
	{
		int diff = 0;
		if(moreY)
		{
			diff++;
		}
		
		
		p.setPosition(this.x+0.5, this.y+diff, this.z+0.5);
	}

}
