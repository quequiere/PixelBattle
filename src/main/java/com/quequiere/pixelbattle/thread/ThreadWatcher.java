package com.quequiere.pixelbattle.thread;

import java.util.ArrayList;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent.ServerTickEvent;

public class ThreadWatcher
{
	private static double last = 0;
	private static ArrayList<Thread> tlist = new ArrayList<Thread>();
	private static ArrayList<Thread> toremove = new ArrayList<Thread>();
	private static ArrayList<Thread> toadd = new ArrayList<Thread>();

	@SubscribeEvent
	public void gestionOuvertureThread(ServerTickEvent e)
	{
		double now = System.currentTimeMillis();
		double diff = now - last;

		if (diff > 1000)
		{
			last = now;
			
			for(Thread t:toremove)
			{
				tlist.remove(t);
			}
			
			for(Thread t:toadd)
			{
				tlist.add(t);
			}
			
			toremove.clear();
			toadd.clear();
			
			for (Thread t : tlist)
			{
				t.run();
			}
		}

	}

	public static void register(Thread t)
	{
		toadd.add(t);
	}

	public static void unregister(Thread t)
	{
		toremove.add(t);
	}
}
